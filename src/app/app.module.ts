import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

//Spread sheet syncfusion module imported here
import { EJAngular2Module } from 'ej-angular2'; 

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
	EJAngular2Module.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
